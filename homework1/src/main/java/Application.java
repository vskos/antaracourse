import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik cat1 = new Kotik();
        Kotik cat2 = new Kotik("Мурзик", "Сибирский Великолепный", "Мя-я-я-я", 5, 7.5, 3);

        System.out.println("Этого котика зовут " + cat1.getName());
        System.out.println("Он весит " + cat1.getWeight() + " килограмм.");
        System.out.println();
        cat1.liveAnotherDay();
        System.out.println();

        if (cat1.getMeow().equals(cat2.getMeow())){
            System.out.println("Котики мяукают одинаково.");
        }
        else{
            System.out.println("Котики мяукают по-разному.");
        }

        System.out.println();
        System.out.println("В процессе выполнения программы было создано " + Kotik.getCountOfKotiks() + " котиков.");

    }
}
