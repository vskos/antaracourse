/*
 * Kotik
 *
 * version 1.0.0
 *
 * мое
 */

package model;

public class Kotik {

    private static int countOfKotiks = 0;
    private String name;
    private String breed;
    private String meow;
    private int age;
    private double weight;
    private int satiety;

    public Kotik(){
        setKotik();
        countOfKotiks++;
    }

    public Kotik(String name, String breed, String meow, int age, double weight, int satiety){
        this.name = name;
        this.breed = breed;
        this.meow = meow;
        this.age = age;
        this.weight = weight;
        this.satiety = satiety;

        countOfKotiks++;
    }


    public static int getCountOfKotiks() {
        return countOfKotiks;
    }

    private void setKotik() {
        name = "Барсик";
        breed = "Подзаборный";
        meow = "Ммм-лл-ее-ааааа!!!";
        age = 13;
        weight = 3.0;
        satiety = 0;
    }

    public String getName(){
        return name;
    }
    public String getBreed(){
        return breed;
    }
    public String getMeow(){
        return meow;
    }
    public int getAge(){
        return age;
    }
    public double getWeight(){
        return weight;
    }
    public int getSatiety() {
        return satiety;
    }

    public boolean isHungry(){
        if (satiety <= 0) return true;
        return false;
    }

    public boolean eat(int portion){
        System.out.println("Котик " + getName() + " жрет как не в себя");
        satiety += portion;
        return true;
    }

    public boolean eat(int portion, String food){
        System.out.println("Котик " + getName() + " жрет как не в себя " + food);
        satiety += portion;
        return true;
    }

    public boolean eat(){
        eat(1, "Вкусняшку");
        return true;
    }

    public boolean sing(){
        if (isHungry()){
            System.out.print("Так хочется петь.... Но еще больше есть!!!....");
            return false;
        }
        System.out.println("Котик " + getName() + " поет: " + getMeow());
        satiety--;
        return true;
    }


    boolean play(){
        if (isHungry()){
            System.out.print("Я бы сейчас поиграл.... Но кушать хочется очень-очень....");
            return false;
        }
        System.out.println("Котик " + getName() + " носится по дому, как сумасшедший, снося все на своем пути.");
        satiety--;
        return true;

    }

    boolean sleep(){
        if (isHungry()){
            System.out.print("С удовольствием посплю... Но только когда ты меня покормишь...");
            return false;
        }
        System.out.println("Котик " + getName() + " мирно спит, сопя в две дырочки.... он вообще живой?");
        satiety--;
        return true;

    }

    boolean lookToWindow(){
        if (isHungry()){
            System.out.print("За окном наверное так красиво.... Но миска пуста... я вижу дно!!!....");
            return false;
        }
        System.out.println("Котик " + getName() + " задумчиво смотрит в окно.... наверное думает о смысле кошачей жизни.");
        satiety--;
        return true;
    }

    boolean space(){
        if (isHungry()){
            System.out.print("У меня сегодня запланирован сеанс связи с космосом.... Но только не на голодный желудок!!!....");
            return false;
        }
        System.out.println("Котик " + getName() + " выходит на связь с космосом... и получает ответ... ура, внеземная разумная жизнь существует!!!");
        satiety--;
        return true;

    }

    public void liveAnotherDay() {
        for (int i = 0; i < 24; i++){
            int n = 5;
            int choice = (int) (Math.random()*n + 1);
            switch (choice) {
                case 1: if (!sleep()){
                    eat();
                };
                break;
                case 2: if (!play()){
                    eat();
                };
                break;
                case 3: if(!sing()){
                    eat();
                };
                break;
                case 4: if(!lookToWindow()){
                    eat();
                };
                break;
                case 5: if(!space()){
                    eat();
                };
                break;

            }
        }

    }


}
