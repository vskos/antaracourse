package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class AvitoPage extends BasePage {

    By categotySelectLocator = By.id("category");
    By searchCheckboxImageOnly = (By.cssSelector("input[name=\"withImagesOnly\"] + span"));
    By searchFieldLocator = By.cssSelector("input[data-marker=\"search-form/suggest\"]");
    By cityFieldLocator = By.cssSelector("div[data-marker=\"search-form/region\"]");
    By cityInputLocator = By.cssSelector("input.suggest-input-rORJM");
    By cityChooseLocator = By.cssSelector("ul.suggest-suggests-CzXfs li:first-child");
    By findBtnLocator = By.cssSelector("button[data-marker=\"popup-location/save-button\"]");

    private Select categorySelect;
    private WebElement checkboxImageOnly;
    private WebElement searchField;
    private WebElement cityField;
    private WebElement cityInput;
    private WebElement cityChoose;

    public AvitoPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void findCategorySelect() {
        this.categorySelect = super.findSomeSelect(categotySelectLocator);
    }

    public void chooseCategory(String name) {
        categorySelect.selectByVisibleText(name);
    }

    public void findCheckboxImageOnly() {
        this.checkboxImageOnly = driver.findElement(searchCheckboxImageOnly);
    }

    public void clickCheckboxImageOnly() {
        if (!checkboxImageOnly.isSelected()) {
            checkboxImageOnly.click();
        }
    }

    public void findSearchField() {
        searchField = driver.findElement(searchFieldLocator);
        searchField.click();
    }

    public void enterSearchText(String findString) {
        searchField.sendKeys(findString);
    }

    public void findCityField() {
        cityField = driver.findElement(cityFieldLocator);
        cityField.click();
    }

    public void chooseCity(String city) {
        cityInput = driver.findElement(cityInputLocator);
        cityInput.sendKeys(city);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        cityChoose = driver.findElement(cityChooseLocator);
        cityChoose.click();
    }

    public void clickFindButton() {
        driver.findElement(findBtnLocator).click();
    }


}
