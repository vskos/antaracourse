package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public abstract class BasePage {

    protected WebDriver driver;

    public Select findSomeSelect(By locator) {
        Select select = new Select(driver.findElement(locator));
        return select;
    }
}
