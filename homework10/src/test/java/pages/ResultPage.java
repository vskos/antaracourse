package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class ResultPage extends BasePage {

    By sortSelectLocator = By.cssSelector("div.index-topPanel-McfCA select");
    By titleLocator = By.xpath("//h1[contains(text(), 'Объявления по')]");
    List<WebElement> goodsList;

    protected WebElement title;
    private Select sortSelect;

    public ResultPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public String getTitleText() {
        title = driver.findElement(titleLocator);
        return title.getText();
    }

    public void findSortingSelect() {
        this.sortSelect = super.findSomeSelect(sortSelectLocator);
    }

    public void chooseSortSelect(String sorting) {
        sortSelect.selectByVisibleText(sorting);
    }

    public void chooseSomeGoodsList(int count) {
        //помещаем в спискок элементов указанное количество предложенных товара
        goodsList = driver.findElements(By.xpath(String.format("//div[@data-marker=\"catalog-serp\"]/div[position()<%d]", count + 1)));
    }

    public void printGoodsList() {
        //выводим в цикле название товара и цену выбранных товаров в консоль
        goodsList.forEach(webElement -> System.out.println(webElement.findElement(By.cssSelector(
                "a[data-marker=\"item-title\"]")).getText() + ". Цена: " + webElement.findElement(By.cssSelector(
                "span[data-marker=\"item-price\"]")).getText()));
    }
}
