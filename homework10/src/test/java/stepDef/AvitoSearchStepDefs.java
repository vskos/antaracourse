package stepDef;

import io.cucumber.java.Before;
import io.cucumber.java.After;
import io.cucumber.java.ParameterType;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Пусть;
import io.cucumber.java.ru.Тогда;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import io.qameta.allure.Step;
import io.qameta.allure.Attachment;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;

import pages.AvitoPage;
import pages.ResultPage;

import javax.imageio.ImageIO;


public class AvitoSearchStepDefs {

    static WebDriver driver;
    AvitoPage avitoPage;
    ResultPage resultPage;
    String product;
    String city;

    private static void takeAshot(String name) throws IOException {
        Screenshot screenshot = new AShot().takeScreenshot(driver);
        File file = new File("src/test/resources/" + name);
        ImageIO.write(screenshot.getImage(), "png", file);
    }

    @Attachment
    public static byte[] getBytes(String resourceName) throws IOException {
//        return Files.readAllBytes(Paths.get("src/main/resources", resourceName));
        return Files.readAllBytes(Paths.get("src/test/resources/", resourceName));
    }

    @Before
    public void setUp() {
        //устанавливаем путь до chromedriver
        System.setProperty("webdriver.chrome.driver", "src/main/driver/chromedriver/chromedriver.exe");
        //создаем экземпляр драйвера, страницы авито, настраиваем неявное ожидание
        driver = new ChromeDriver();
        avitoPage = new AvitoPage(driver);
        resultPage = new ResultPage(driver);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    }

    @Step("Открыт ресурс авито")
    @Пусть("открыт ресурс авито")
    public void открытРесурсАвито() throws IOException {
        //переходим на страницу авито
        driver.get("https://www.avito.ru");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.navigate().refresh();
        takeAshot("ashot0.png");
        getBytes("ashot0.png");
    }

    @ParameterType(".*")
    public GoodsCategories categories(String category) {
        return GoodsCategories.valueOf(category);
    }
    @Step("В выпадающем списке категорий выбрана {category}")
    @И("в выпадающем списке категорий выбрана {categories}")
    public void вВыпадающемСпискеКатегорийВыбранаОргтехника(GoodsCategories category) throws IOException {
        avitoPage.findCategorySelect();
        avitoPage.chooseCategory(category.getName());
        takeAshot("ashot1.png");
        getBytes("ashot1.png");
    }
    @Step("Активирован чекбокс только с фотографией")
    @И("активирован чекбокс только с фотографией")
    public void активированЧекбоксТолькоСФотографией() throws IOException {
        //находим чекбокс "только с фотографией" и, если он не выбран, кликаем на него
        avitoPage.findCheckboxImageOnly();
        avitoPage.clickCheckboxImageOnly();
        takeAshot("ashot2.png");
        getBytes("ashot2.png");
    }
    @Step("В поле поиска введено значение {findString}")
    @И("в поле поиска введено значение {word}")
    public void вПолеПоискаВведеноОпределенноеЗначение(String findString) throws IOException {
        avitoPage.findSearchField();
        avitoPage.enterSearchText(findString);
        product = findString;
        takeAshot("ashot3.png");
        getBytes("ashot3.png");
    }
    @Step("Кликнуть по выпадающему списку региона")
    @Тогда("кликнуть по выпадающему списку региона")
    public void кликнутьПоВыпадающемуСпискуРегиона() throws IOException {
        //кликаем по полю выбора города
        avitoPage.findCityField();
        takeAshot("ashot4.png");
        getBytes("ashot4.png");
    }
    @Step("В поле регион введено значение {city}")
    @Тогда("в поле регион введено значение {word}")
    public void вПолеРегионВведеноОпределенноеЗначение(String city) throws IOException {
        // вводим название города и кликаем по первому варианту
        avitoPage.chooseCity(city);
        this.city = city;
        takeAshot("ashot5.png");
        getBytes("ashot5.png");
    }
    @Step("Нажата кнопка показать объявления")
    @И("нажата кнопка показать объявления")
    public void нажатаКнопкаПоказатьОбъявления() throws IOException {
        //нажимаем кнопку "Найти"
        avitoPage.clickFindButton();
        takeAshot("ashot6.png");
        getBytes("ashot6.png");
    }
    @Step("Открылась страница результаты по запросу {requestWord}")
    @Тогда("открылась страница результаты по запросу {word}")
    public void открыласьСтраницаРезультатыПоЗапросуПринтер(String requestWord) throws IOException {
        String titleText = resultPage.getTitleText();
        takeAshot("ashot7.png");
        getBytes("ashot7.png");
        Assert.assertEquals(titleText, String.format("Объявления по запросу «%s» во %sе", requestWord, city), "Отображена другая страница");

    }

    @ParameterType(".*")
    public SortType sort(String sortType) {
        return SortType.valueOf(sortType);
    }
    @Step("В выпадающем списке сортировка выбрано значение {sorting}")
    @И("в выпадающем списке сортировка выбрано значение {sort}")
    public void вВыпадающемСпискеСортировкаВыбраноОпределенноеЗначение(SortType sorting) throws IOException {
        //выбираем в выпадающем списке сортировки соответствующее значение
        resultPage.findSortingSelect();
        resultPage.chooseSortSelect(sorting.getText());
        takeAshot("ashot8.png");
        getBytes("ashot8.png");
    }
    @Step("В консоль выведено значение названия и цены {count} первых товаров")
    @И("в консоль выведено значение названия и цены {int} первых товаров")
    public void вКонсольВыведеноЗначениеНазванияИЦеныПервыхТоваров(int count) throws IOException {
        //помещаем в спискок элементов определенное количество предложенных товара
        resultPage.chooseSomeGoodsList(count);
        takeAshot("ashot9.png");
        getBytes("ashot9.png");
        //выводим в цикле название товара и цену выбранных товаров в консоль
        resultPage.printGoodsList();
    }
    @After
    public void teardown() {
        //закрываем браузер
        driver.quit();
    }
}
