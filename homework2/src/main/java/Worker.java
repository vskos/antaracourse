import animals.Animal;
import animals.Voice;
import food.Food;

public class Worker {

    private String name;

    public Worker(String name){
        this.name = name;
    }

    public void feed(Animal animal, Food food){
        System.out.println("Работник " + name + " дает еду " + food.getName() + " => " + animal.getName());
        animal.eat(food);
    }

    public void getVoice(Voice animal){
        System.out.println("Работник " + name + " пытается заставить " + animal.toString() + " подать голос.");
        System.out.println(animal.toString() + " " + animal.voice());
    }

    public String getName() {
        return name;
    }
}
