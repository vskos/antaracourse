import animals.*;
import food.Food;
import food.Grass;
import food.Meat;

public class Zoo {

    public static void main(String[] args) {

        Worker worker = new Worker("Дима");

        Lion lion1 = new Lion("Дмитрий");
        Deer deer1 = new Deer("Вася");
        Duck duck1 = new Duck("Саша");
        Duck duck2 = new Duck("Анатолий");
        Eagle eagle1 = new Eagle("Вячеслав");
        Pig pig1 = new Pig("Наташа");
        Fish fish1 = new Fish("Коля");
        Fish fish2 = new Fish("Арман");

        Food meat1 = new Meat("Окорок хрюшки", 3);
        Food meat2 = new Meat("Куринные ножки", 2);
        Food grass1 = new Grass("Большая морковка", 1);
        Food grass2 = new Grass("Спелые яблоки", 2);

        lion1.run();
        deer1.run();
        duck1.fly();
        eagle1.fly();
        pig1.run();

        System.out.println();

        worker.feed(lion1, meat1);
        worker.feed(lion1, grass1);
        worker.feed(deer1, meat2);
        worker.feed(deer1, grass2);

        System.out.println();

        worker.getVoice(lion1);
        //worker.getVoice(deer1);   // Метод getVoice класса Worker не должен принимать экземпляры класса Fish и других
                                    // немых животных (при попытке это сделать программа не должна компилироваться).
        worker.getVoice(duck1);
        //worker.getVoice(eagle1);  // Метод getVoice класса Worker не должен принимать экземпляры класса Fish и других
                                    // немых животных (при попытке это сделать программа не должна компилироваться).
        worker.getVoice(pig1);
        //worker.getVoice(fish1);   // Метод getVoice класса Worker не должен принимать экземпляры класса Fish и других
                                    // немых животных (при попытке это сделать программа не должна компилироваться).

        System.out.println();

        Swim[] pond  = {fish1, fish2, duck1, duck2};
        System.out.println("Создан пруд с четырьмя животными.");
        for (Swim obj : pond){
            obj.swim();
        }

        System.out.println();

        lion1.showSatiety();
        deer1.showSatiety();
        duck1.showSatiety();
        pig1.showSatiety();
        eagle1.showSatiety();
        fish1.showSatiety();
    }
}
