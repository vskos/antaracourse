package animals;

import food.Food;

public abstract class Animal {

    protected String name;
    protected int satiety;

    public String toString(){
        return name;
    }

    public String getName(){ return name; }

    public void showSatiety(){
        System.out.println("Степень сытости " + getName() + " равна " + satiety);
    }

    public abstract void eat(Food food);

}
