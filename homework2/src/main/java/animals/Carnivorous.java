package animals;
import food.Food;
import food.Meat;

public abstract class Carnivorous extends Animal{

    @Override
    public void eat(Food food) {
        if (food instanceof Meat){
            System.out.println(getName() + " кушает " + food.getName());
            satiety += food.getSaturation();
        } else {
            System.out.println(getName() + " не собирается жевать эту траву");
        }
    }
}
