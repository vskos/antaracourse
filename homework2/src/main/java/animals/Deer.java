package animals;

public class Deer extends Herbivore implements Run{

    public Deer(String name){
        this.name = "Олень " + name;
        satiety = 0;
    }

    @Override
    public void run() {
        System.out.println(getName() + " Скачет по зоопарку не прекращая.");
        satiety--;
    }

}
