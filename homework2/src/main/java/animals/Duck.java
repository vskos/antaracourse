package animals;

public class Duck extends Herbivore implements Fly, Swim, Voice{

    public Duck(String name){
        this.name ="Утка " + name;
        satiety = 0;
    }

    @Override
    public void fly() {
        System.out.println(getName() + " красиво летает над зоопарком.");
        satiety--;
    }

    @Override
    public void swim(){
        System.out.println(getName() + " плавает по поверхности пруда, иногда заныривая под воду.");
        satiety--;
    }

    @Override
    public String voice() {
        return "кричит кря-кря. ";
    }
}
