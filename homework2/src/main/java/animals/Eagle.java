package animals;

public class Eagle extends Carnivorous implements Fly{

    public Eagle(String name) {
        this.name = "Орел " + name;
        satiety = 0;
    }

    @Override
    public void fly() {
        System.out.println(getName() + " гордо кружит над зоопарком, высматривая добычу. ");
        satiety--;
    }
}
