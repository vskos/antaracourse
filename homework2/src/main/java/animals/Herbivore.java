package animals;
import food.Food;
import food.Grass;

public abstract class Herbivore extends Animal {

    @Override
    public void eat(Food food) {
        if (food instanceof Grass){
            System.out.println(getName() + " кушает " + food.getName());
            satiety += food.getSaturation();

        } else {
            System.out.println(getName() + " не будет есть чей-то труп");
        }
    }
}
