package animals;

public class Lion extends Carnivorous implements Run, Voice{

    public Lion(String name) {
        this.name = "Лев " + name;
        satiety = 0;
    }

    @Override
    public void run() {
        System.out.println(getName() + " бегает по зоопарку за другими зверями.");
        satiety--;
    }

    @Override
    public String voice() {
        return "громко рычит \"А-А-А-А-ГГ-РРР\"";
    }
}
