package food;

public abstract class Food {
    private String name;
    private int saturation;

    public Food (String name, int saturation){
        this.name = name;
        this.saturation = saturation;
    }

    public String getName() {
        return name;
    }

    public int getSaturation() {
        return saturation;
    }
}
