import actionsInterface.Swim;
import animals.*;
import food.Food;
import food.Grass;
import food.Meat;

public class Zoo {

    public static void main(String[] args) {

        Worker worker = new Worker("Дима");

        Lion lion1 = new Lion("Дмитрий");
        Deer deer1 = new Deer("Вася");
        Duck duck1 = new Duck("Саша");
        Duck duck2 = new Duck("Анатолий");
        Eagle eagle1 = new Eagle("Вячеслав");
        Pig pig1 = new Pig("Наташа");
        Fish fish1 = new Fish("Коля");
        Fish fish2 = new Fish("Арман");

        Food meat1 = new Meat("Окорок хрюшки", 3);
        Food meat2 = new Meat("Куринные ножки", 2);
        Food grass1 = new Grass("Большая морковка", 1);
        Food grass2 = new Grass("Спелые яблоки", 2);

        lion1.run();
        deer1.run();
        duck1.fly();
        eagle1.fly();
        pig1.run();

        System.out.println();

        worker.feed(lion1, meat1);
        worker.feed(lion1, grass1);
        worker.feed(deer1, meat2);
        worker.feed(deer1, grass2);

        System.out.println();

        worker.getVoice(lion1);
        //worker.getVoice(deer1);   // Метод getVoice класса Worker не должен принимать экземпляры класса Fish и других
        // немых животных (при попытке это сделать программа не должна компилироваться).
        worker.getVoice(duck1);
        //worker.getVoice(eagle1);  // Метод getVoice класса Worker не должен принимать экземпляры класса Fish и других
        // немых животных (при попытке это сделать программа не должна компилироваться).
        worker.getVoice(pig1);
        //worker.getVoice(fish1);   // Метод getVoice класса Worker не должен принимать экземпляры класса Fish и других
        // немых животных (при попытке это сделать программа не должна компилироваться).

        System.out.println();

        Swim[] pond = {fish1, fish2, duck1, duck2};
        System.out.println("Создан пруд с четырьмя животными.");
        for (Swim obj : pond) {
            obj.swim();
        }

        System.out.println();

        lion1.showSatiety();
        deer1.showSatiety();
        duck1.showSatiety();
        pig1.showSatiety();
        eagle1.showSatiety();
        fish1.showSatiety();

        System.out.println();

        System.out.println("Создаем самый большой вальер для хищников");
        Enclosure<Carnivorous> enclosure1 = new Enclosure<>(Capacity.BIGGEST);
        enclosure1.addAnimal(lion1);
//         enclosure1.addAnimal(deer1);  // приведет к исключению, тип животного не соответствует типу вальера
        enclosure1.addAnimal(eagle1);
        enclosure1.addAnimal(fish1);
        enclosure1.showAnimals();


        System.out.println();

        System.out.println("Создаем средний вальер для хищников");
        Enclosure<Carnivorous> enclosure2 = new Enclosure<>(Capacity.MEDIUM);
        enclosure2.addAnimal(lion1);
        enclosure2.addAnimal(eagle1);
        enclosure2.addAnimal(fish1);
//        enclosure2.addAnimal(duck1);   // приведет к исключению, тип животного не соответствует типу вальера
        enclosure2.showAnimals();

        System.out.println();

        System.out.println("Создаем средний вальер для травоядных");
        Enclosure<Herbivore> enclosure3 = new Enclosure<>(Capacity.MEDIUM);
        enclosure3.addAnimal(deer1);
        enclosure3.addAnimal(duck1);
        enclosure3.addAnimal(pig1);
//        enclosure3.addAnimal(lion1);   // приведет к исключению, тип животного не соответствует типу вальера
        enclosure3.showAnimals();

        System.out.println();

        System.out.println("Создаем небольшой вальер для травоядных");
        Enclosure<Herbivore> enclosure4 = new Enclosure<>(Capacity.SMALL);
        enclosure4.addAnimal(deer1);
        enclosure4.addAnimal(duck1);
        enclosure4.addAnimal(pig1);
//        enclosure4.addAnimal(lion1);   // приведет к исключению, тип животного не соответствует типу вальера
        enclosure4.showAnimals();

        System.out.println();
        System.out.println("Получаем по имени объект из большого вальера для хищников и присваеваем его переменной ");
        Animal eagle2 = enclosure1.getAnimal("Орел Вячеслав");
        System.out.println("Выводим на экран имя извлеченного из вальера животного");
        System.out.println(eagle2);
        System.out.println("Удаляем из вальера животное по ссылке на данный объект ");
        enclosure1.removeAnimal(lion1);
        System.out.println("Выводим на экран оставшихся в вальере животных");
        enclosure1.showAnimals();

        System.out.println();
        Lion lion3 = new Lion("Дмитрий");
        Deer deer3 = new Deer("Вася");
        Duck duck3 = new Duck("Саша");
        Eagle eagle3 = new Eagle("Вячеслав");
        Pig pig3 = new Pig("Наташа");
        Fish fish3 = new Fish("Коля");

        System.out.println("hashcode lion1 = " + lion1.hashCode());
        System.out.println("hashcode lion3 = " + lion3.hashCode());
        System.out.println("Проверка равенства lion1 and lion3: " + lion1.equals(lion3));

    }
}
