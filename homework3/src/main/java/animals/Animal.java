package animals;

import food.Food;

public abstract class Animal {

    protected String name;
    protected int satiety;
    protected Capacity enclosureSize;

    @Override
    public String toString() {
        return name;
    }

    public Capacity getEnclosureSize() {
        return enclosureSize;
    }

    public String getName() {
        return name;
    }

    public void showSatiety() {
        System.out.println("Степень сытости " + getName() + " равна " + satiety);
    }

    public abstract void eat(Food food);

    @Override
    public boolean equals(Object obj_animal) {
        Animal animal = (Animal) obj_animal;
        if (this == animal) return true;
        if (this.getClass() != animal.getClass()) return false;
        return (this.getName().equals(animal.getName())) & this.getEnclosureSize().equals(animal.getEnclosureSize());
    }

    @Override
    public int hashCode() {
        return name.hashCode() + enclosureSize.hashCode();
    }

}
