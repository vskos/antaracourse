package animals;

public enum Capacity {
    BIGGEST, BIG, MEDIUM, SMALL
}
