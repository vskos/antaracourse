package animals;

import actionsInterface.Run;

public class Deer extends Herbivore implements Run {

    public Deer(String name) {
        this.name = "Олень " + name;
        satiety = 0;
        enclosureSize = Capacity.BIGGEST;
    }

    @Override
    public void run() {
        System.out.println(getName() + " Скачет по зоопарку не прекращая.");
        satiety--;
    }

}
