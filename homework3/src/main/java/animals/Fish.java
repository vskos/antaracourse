package animals;

import actionsInterface.Swim;

public class Fish extends Carnivorous implements Swim {

    public Fish(String name) {
        this.name = "Рыба " + name;
        satiety = 0;
        enclosureSize = Capacity.SMALL;
    }

    @Override
    public void swim() {
        System.out.println(getName() + " плавает в пруду как рыба в воде.");
        satiety--;
    }
}
