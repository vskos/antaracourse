package animals;

import actionsInterface.Run;
import actionsInterface.Voice;

public class Pig extends Herbivore implements Run, Voice {

    public Pig(String name) {
        this.name = "Хрюша " + name;
        satiety = 0;
        enclosureSize = Capacity.MEDIUM;
    }

    @Override
    public void run() {
        System.out.println(getName() + " носится по зоопарку как сумасшедшая.");
        satiety--;
    }

    @Override
    public String voice() {
        return "весело хрюкает \"хрю-хрю\"";
    }
}
