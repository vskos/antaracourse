package food;

public class WrongFoodException extends Exception {

    public WrongFoodException() {
        super("Неподходящая еда для этого животного. ");
    }

}
