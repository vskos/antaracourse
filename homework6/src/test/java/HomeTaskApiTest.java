import inventoryMap.Inventory;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.Random;
import order.Endpoints;
import order.Order;


import static io.restassured.RestAssured.given;


public class HomeTaskApiTest {

    @BeforeClass
    static void setUp() {
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri(Endpoints.host + Endpoints.orderBasePath)
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();
        RestAssured.filters(new ResponseLoggingFilter());
    }

    private Order buildOrder() {
        Order order = new Order();
        order.setId(new Random().nextInt(10));
        order.setPetId(new Random().nextInt(10000));
        order.setQuantity(new Random().nextInt(10));
        return order;
    }

    private void postOrder(Order order) {
        System.out.println("\nСоздаем заказ\n");
        given()
                .body(order)
                .when()
                .post(Endpoints.orderPath)
                .then()
                .statusCode(200);
    }

    private Order getOrder(int id) {
        System.out.println("\nПолучаем объект заказа\n");
        return given().pathParam("id", id)
                .when().get(Endpoints.orderPath + Endpoints.orderId)
                .then().assertThat()
                .statusCode(200).extract().body().as(Order.class);
    }

    private void deleteOrder(int id){
        System.out.println("\nУдаляем заказ\n");
        given().pathParam("id", id)
                .when().delete(Endpoints.orderPath + Endpoints.orderId)
                .then().assertThat()
                .statusCode(200);
    }

    private int getResponseCode(int id){
        System.out.println("\nПолучаем код ответа метода GET\n");
        return given().pathParam("id", id)
                .when().get(Endpoints.orderPath + Endpoints.orderId)
                .then().extract().response().getStatusCode();
    }

    private Inventory getInvetory(){
        System.out.println("Получаем Inventory");
        return given()
                .when().get(Endpoints.inventoryPath)
                .then().assertThat()
                .statusCode(200).extract().body().as(Inventory.class);
    }

    @Test
    public void postOrderTest() {
        System.out.println("\nПроверка метода POST и GET\n");
        Order order = buildOrder();
        postOrder(order);
        Order postedOrder = getOrder(order.getId());
        Assert.assertEquals(order, postedOrder);
        }

    @Test
    public void deleteOrderTest() {
        System.out.println("\nПроверка метода DELETE\n");
        Order order = buildOrder();
        postOrder(order);
        deleteOrder(order.getId());
        Assert.assertEquals(getResponseCode(order.getId()), 404);
    }

    @Test
    public void getInventoryTest() {
        System.out.println("\nПроверка метода GET Inventory\n");
        Inventory inventory = getInvetory();
        Assert.assertTrue(inventory.containsKey("available"), "Inventory не содержит ключ available" );

    }
}
