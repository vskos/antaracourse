package order;

public final class Endpoints {
    public static String host = "https://petstore.swagger.io";
    //public static String host = "http://213.239.217.15:9090";
    public static String orderBasePath = "/v2/store";
    //public static String orderBasePath = "/api/v3/store";
    public static String inventoryPath = "/inventory";
    public static String orderPath = "/order";
    public static String orderId = "/{id}";
}
