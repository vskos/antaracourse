package order;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;



public class Order{

    @SerializedName("petId")
    private int petId;

    @SerializedName("quantity")
    private int quantity;

    @SerializedName("id")
    private int id;

    @SerializedName("shipDate")
    private String shipDate;

    @SerializedName("complete")
    private boolean complete;

    @SerializedName("status")
    private String status;

    public int getPetId(){
        return petId;
    }

    public int getQuantity(){
        return quantity;
    }

    public int getId(){
        return id;
    }

    public String getShipDate(){
        return shipDate;
    }

    public boolean isComplete(){
        return complete;
    }

    public String getStatus(){
        return status;
    }

    public void setPetId(int petId) {
        this.petId = petId;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setShipDate(String shipDate) {
        this.shipDate = shipDate;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return petId == order.petId && quantity == order.quantity && id == order.id && complete == order.complete && Objects.equals(shipDate, order.shipDate) && Objects.equals(status, order.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(petId, quantity, id, shipDate, complete, status);
    }

    @Override
    public String toString(){
        return
                "Order{" +
                        "id = '" + id + '\'' +
                        ", petId = '" + petId + '\'' +
                        ", quantity = '" + quantity + '\'' +
                        ", shipDate = '" + shipDate + '\'' +
                        ", status = '" + status + '\'' +
                        ", complete = '" + complete + '\'' +
                        "}";
    }

}