import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;
import java.util.List;

public class browseAvito {
    public static void main(String[] args) {
        //устанавливаем путь до chromedriver
        System.setProperty("webdriver.chrome.driver", "homework7/src/main/driver/chromedriver/chromedriver.exe");
        //создаем экземпляр драйвера и настраиваем неявное ожидание
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //переходим на страницу авито
        driver.get("https://www.avito.ru");
        //открываем выпадающий список категорий объявлений и выбираем "Оргтехника и расходники"
        Select categorySelect = new Select(driver.findElement(By.id("category")));
        categorySelect.selectByVisibleText("Оргтехника и расходники");
        //вводим в поисковое поле текст "Принтер"
        WebElement findField = driver.findElement(By.cssSelector("input[data-marker=\"search-form/suggest\"]"));
        findField.click();
        findField.sendKeys("Принтер");
        //кликаем по полю выбора города, вводим "Владивосток" и кликаем по первому варианту
        WebElement cityField = driver.findElement(By.cssSelector("div[data-marker=\"search-form/region\"]"));
        cityField.click();
        WebElement cityInput = driver.findElement(By.cssSelector("input.suggest-input-rORJM"));
        cityInput.sendKeys("Владивосток");
        WebElement cityChoose = driver.findElement(By.cssSelector("ul.suggest-suggests-CzXfs li:first-child"));
        cityChoose.click();
        //нажимаем кнопку "Найти"
        driver.findElement(By.cssSelector("button[data-marker=\"popup-location/save-button\"]")).click();
        //находим чекбокс "С Авито Доставкой" и, если он не выбран, кликаем на него
        WebElement deliveryCheckbox = driver.findElement(By.cssSelector("span[data-marker=\"delivery-filter/text\"]"));
        if (!deliveryCheckbox.isSelected()) {deliveryCheckbox.click();}
        //нажимаем на кнопку "Показать объявления"
        driver.findElement(By.cssSelector("button[data-marker=\"search-filters/submit-button\"]")).click();
        //выбираем в выпадающем списке сортировки значение "Дороже"
        Select sortSelect = new Select(driver.findElement(By.cssSelector("div.index-topPanel-McfCA select")));
        sortSelect.selectByVisibleText("Дороже");
        //помещаем в спискок элементов первые три предложенных товара
        List<WebElement> goodsList = driver.findElements(By.xpath("//div[@data-marker=\"catalog-serp\"]/div[position()<4]"));
        //выводим в цикле название товара и цену выбранных товаров в консоль
        goodsList.forEach(webElement -> System.out.println(
                webElement.findElement(By.cssSelector("a[data-marker=\"item-title\"]")).getText() + ". Цена: " +
                webElement.findElement(By.cssSelector("span[data-marker=\"item-price\"]")).getText()));

        //закрываем браузер
        driver.close();
    }
}
