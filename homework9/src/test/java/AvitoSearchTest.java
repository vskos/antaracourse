import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = "src/test/java/features",//путь до feature файлов
        glue = "stepDef",//название пакета с шагами
        tags = "@1" //Теги по которым будет запускаться сценарий
)
@SuppressWarnings("All")
public class AvitoSearchTest extends AbstractTestNGCucumberTests {
}