package stepDef;

import io.cucumber.java.Before;
import io.cucumber.java.After;
import io.cucumber.java.ParameterType;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Пусть;
import io.cucumber.java.ru.Тогда;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import java.time.Duration;
import pages.AvitoPage;
import pages.ResultPage;

public class AvitoSearchStepDefs {

    WebDriver driver;
    AvitoPage avitoPage;
    ResultPage resultPage;
    String product;
    String city;

    @Before
    public void setUp() {
        //устанавливаем путь до chromedriver
        System.setProperty("webdriver.chrome.driver", "src/main/driver/chromedriver/chromedriver.exe");
        //создаем экземпляр драйвера, страницы авито, настраиваем неявное ожидание
        driver = new ChromeDriver();
        avitoPage = new AvitoPage(driver);
        resultPage = new ResultPage(driver);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    }

    @Пусть("открыт ресурс авито")
    public void открытРесурсАвито() {
        //переходим на страницу авито
        driver.get("https://www.avito.ru");
    }

    @ParameterType(".*")
    public GoodsCategories categories(String category) {
        return GoodsCategories.valueOf(category);
    }

    @И("в выпадающем списке категорий выбрана {categories}")
    public void вВыпадающемСпискеКатегорийВыбранаОргтехника(GoodsCategories category) {
        avitoPage.findCategorySelect();
        avitoPage.chooseCategory(category.getName());
    }

    @И("активирован чекбокс только с фотографией")
    public void активированЧекбоксТолькоСФотографией() {
        //находим чекбокс "только с фотографией" и, если он не выбран, кликаем на него
        avitoPage.findCheckboxImageOnly();
        avitoPage.clickCheckboxImageOnly();
    }

    @И("в поле поиска введено значение {word}")
    public void вПолеПоискаВведеноОпределенноеЗначение(String findString) {
        avitoPage.findSearchField();
        avitoPage.enterSearchText(findString);
        product = findString;
    }


    @Тогда("кликнуть по выпадающему списку региона")
    public void кликнутьПоВыпадающемуСпискуРегиона() {
        //кликаем по полю выбора города
        avitoPage.findCityField();
    }

    @Тогда("в поле регион введено значение {word}")
    public void вПолеРегионВведеноОпределенноеЗначение(String city) {
        // вводим название города и кликаем по первому варианту
        avitoPage.chooseCity(city);
        this.city = city;
    }

    @И("нажата кнопка показать объявления")
    public void нажатаКнопкаПоказатьОбъявления() {
        //нажимаем кнопку "Найти"
        avitoPage.clickFindButton();
    }

    @Тогда("открылась страница результаты по запросу {word}")
    public void открыласьСтраницаРезультатыПоЗапросуПринтер(String requestWord) {
        String titleText = resultPage.getTitleText();
        Assert.assertEquals(titleText, String.format("Объявления по запросу «%s» во %sе", requestWord, city), "Отображена другая страница");
    }

    @ParameterType(".*")
    public SortType sort(String sortType) {
        return SortType.valueOf(sortType);
    }

    @И("в выпадающем списке сортировка выбрано значение {sort}")
    public void вВыпадающемСпискеСортировкаВыбраноОпределенноеЗначение(SortType sorting) {
        //выбираем в выпадающем списке сортировки соответствующее значение
        resultPage.findSortingSelect();
        resultPage.chooseSortSelect(sorting.getText());
    }

    @И("в консоль выведено значение названия и цены {int} первых товаров")
    public void вКонсольВыведеноЗначениеНазванияИЦеныПервыхТоваров(int count) {
        //помещаем в спискок элементов определенное количество предложенных товара
        resultPage.chooseSomeGoodsList(count);
        //выводим в цикле название товара и цену выбранных товаров в консоль
        resultPage.printGoodsList();
    }

    @After
    public void teardown() {
        //закрываем браузер
        driver.close();
    }

}
